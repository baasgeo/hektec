VERSION=0.7.0
USR=pyftp
sudo mkdir /opt/pyftpd-${VERSION}
sudo virtualenv --system-site-packages /opt/pyftpd-${VERSION}/env
sudo cp *.py /opt/pyftpd-${VERSION}
sudo /opt/pyftpd-${VERSION}/env/bin/pip install -r requirements.txt
sudo chown $USR -R /opt/pyftpd-${VERSION}

sudo rm /opt/pyftpd
sudo ln -s /opt/pyftpd-${VERSION} /opt/pyftpd

sudo systemctl restart ftp.service