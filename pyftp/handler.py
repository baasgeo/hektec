import logging
import os

from pyftpdlib.handlers import FTPHandler
from csvchecker import CSVChecker

class MyHandler(FTPHandler):

    def on_connect(self):
        logging.info("%s:%s connected" % (self.remote_ip, self.remote_port))

    """
    def on_disconnect(self):
        # do something when client disconnects
        pass

    def on_login(self, username):
        # do something when user login
        pass

    def on_logout(self, username):
        # do something when user logs out
        pass

    def on_file_sent(self, file):
        # do something when a file has been sent
        pass

    def on_incomplete_file_sent(self, file):
        # do something when a file is partially sent
        pass
    """

    def on_file_received(self, file):
        # do something when a file has been received
        newFile = CSVChecker(file)
        newFile.parse()
        newFile.exit()
        pass

    def on_incomplete_file_received(self, file):
        # remove partially uploaded files
        import os
        os.remove(file)