import os, csv
from collections import OrderedDict
import logging
import petl as etl

from postgres import pgWriter


class CSVChecker:
    pg = None
    delimiter = ','
    archiveDir = '_archive'
    dbFields = 'sensor_id', 'index', 'run', 'duration', 'east', 'north', 'elevation', 'filename'
    mappings = OrderedDict([
        ('_name', 'Naam'),
        ('_date', 'Datum sessie'),
        ('_time', 'Tijd Sessie'),
        ('_duration', 'Verlopen tijd'),
        ('index', 'Sessie'),
        ('east', 'Oost/'),
        ('north', 'Noord/'),
        ('elevation', 'Hoogte/'),
        ('code', 'Code'),
        ('description', 'Beschrijv.')
    ])

    def __init__(self, file):
        self.file = file
        self.dict = {}

    def check(self):
        logging.info('Checking file %s' % self.file)
        fileName, fileExtension = os.path.splitext(self.file)
        if (fileExtension == '.txt'): #(fileExtension == '.csv' or fileExtension == '.txt'):
            logging.info('Valid extension %s' % fileExtension)
            # self.__checkHeaders(self.file)
            return True
        else:
            logging.info('Format is not valid')
            return False

    def parse(self):
        if self.check():
            self.pg = pgWriter()
            path = os.path.dirname(self.file)

            folder = os.path.basename(path).split('-')
            if len(folder) > 1:
                project, appliance = os.path.basename(path).split('-')
                projectId = self.pg.getProjectId(project)
                applianceId = self.pg.getApplianceId(projectId, appliance)
            else:
                project = os.path.basename(path)
                projectId = self.pg.getProjectId(project)
                applianceId = None

            if projectId:
                try:
                    logging.info('Found projectId %s for project %s', projectId, project)
                    self.__checkSensors(self.file, projectId, applianceId)
                    self.__doETL(self.file, projectId)
                    self.pg.doMaintenance()
                    if self._checkArchiveDir(path + '/' + self.archiveDir):
                        fileName = os.path.basename(self.file)
                        logging.debug(path + '/' + self.archiveDir + '/' + fileName)
                        os.rename(self.file, path + '/' + self.archiveDir + '/' + fileName)
                    logging.info('Succesfull import of file %s' % self.file)
                except Exception as e:
                    logging.error(e)
                    logging.warning('Could not import file %s' % self.file)

    def exit(self):
        self.pg.exit()

    def _checkArchiveDir(self, dir):
        if not os.path.exists(dir):
            os.makedirs(dir)
            return True
        else:
            return True

    def __checkHeaders(self, file):
        with open(file) as csvfile:
            reader = csv.reader(csvfile)
            headers = reader.next()
        print 'ToDo: check headers'

    def __checkSensors(self, file, projectId, applianceId):
        newTable = (etl
                    .fromcsv(file)
                    .fieldmap(self.mappings)
                    .distinct('_name')
                    .rename('_name', 'identity')
                    .addfield('project_id', projectId)
                    .addfield('appliance_id', applianceId)
                    .convert('identity', int)
                    .cut('identity', 'project_id', 'appliance_id', 'code', 'description', 'east', 'north', 'elevation')
                    )
        logging.debug('Checking sensors %s: ', etl.dicts(newTable),)
        logging.info('Inserting any new sensors...')
        self.pg.upsert(newTable, 'sensor')

    def __doETL(self, file, projectId):
        print '__doETL'
        isotime = etl.timeparser('%H:%M:%S:%f')
        isodatetime = etl.datetimeparser('%d-%m-%Y:%H:%M:%S')
        fileName = os.path.basename(file)

        projectTable = etl.fromdb(self.pg.CONN, 'SELECT * FROM monitoring.sensor WHERE project_id = %s' % projectId)
        lkp = etl.lookupone(projectTable, ('project_id', 'identity'), 'id')

        newTable = (
            etl
                .fromcsv(file)
                .fieldmap(self.mappings)
                .convert('_name', int)
                .addfield('filename', fileName)
                .addfield('run', lambda row: isodatetime(row['_date'] + ':' + row['_time']))
                .addfield('duration', lambda row: isotime(row['_duration']))
                .addfield('sensor_id', lambda row: lkp[(projectId, row['_name'])])
                .cut(self.dbFields)
        )
        logging.info('Append measure data to table...')
        self.pg.upsert(newTable, 'deformation')
