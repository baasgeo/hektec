import getpass
import psycopg2
import logging
from petl.io.db_utils import _quote, _placeholders
from petl.compat import text_type

class pgWriter:
    CONN = None
    SCHEMA = 'monitoring'
    DBNAME = 'hektec'
    KIND = 'deformation'
    USER = getpass.getuser()
    ROLE = 'pgr_authenticator'
    SQL_INSERT_QUERY = 'INSERT INTO %s (%s) VALUES (%s) ON CONFLICT DO NOTHING;'

    def __init__(self):
        self.__connect()

    def __connect(self):
        try:
            logging.info('Connecting to database...')
            self.CONN = psycopg2.connect('dbname=%s user=%s' % (self.DBNAME, self.USER))
            cur = self.CONN.cursor()
            cur.execute('SET ROLE %s', (self.ROLE,))
            cur.close()
            return True
        except psycopg2.DatabaseError, exception:
            logging.error(exception)
            return False

    def exit(self):
        if self.CONN:
            logging.info('Disconnecting')
            self.CONN.close()

    def getProjectId(self, projectIdentity):
        cur = self.CONN.cursor()
        cur.execute('SELECT id FROM monitoring.project WHERE identity = %s LIMIT 1;', (projectIdentity,))
        result = cur.fetchone()
        cur.close()
        if result is None:
            return self.setProject(projectIdentity)
        else:
            return result[0]

    def setProject(self, projectIdentity):
        logging.info('Inserting new project...')
        try:
            cur = self.CONN.cursor()
            cur.execute('INSERT INTO monitoring.project (identity) VALUES (%s) ON CONFLICT DO NOTHING RETURNING id;', (projectIdentity,))
            projectId = cur.fetchone()[0]
            cur.close()
            self.CONN.commit()
            return projectId
        except psycopg2.DatabaseError, exception:
            self.CONN.rollback()
            logging.error(exception)
            return None

    def getApplianceId(self, projectId, applianceIdentity):
        cur = self.CONN.cursor()
        cur.execute('SELECT id FROM monitoring.appliance WHERE identity = %s AND project_id = %s LIMIT 1;', (applianceIdentity,projectId))
        result = cur.fetchone()
        cur.close()
        if result is None:
            return self.setAppliance(projectId, applianceIdentity)
        else:
            return result[0]

    def setAppliance(self, projectId, applianceIdentity):
        logging.info('Inserting new appliance...')
        try:
            cur = self.CONN.cursor()
            cur.execute('INSERT INTO monitoring.appliance (identity, project_id, kind) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING RETURNING id;', (applianceIdentity, projectId, self.KIND))
            applianceId = cur.fetchone()[0]
            cur.close()
            self.CONN.commit()
            return applianceId
        except psycopg2.DatabaseError, exception:
            self.CONN.rollback()
            logging.error(exception)
            return None

    def doMaintenance(self):
        logging.info('Updating materialized views...')
        ignores = []
        cur = self.CONN.cursor()

        # get all the materalized views and then loop
        get_all_mv_SQL = """SELECT oid::regclass::text as name
                            FROM   pg_class
                            WHERE  relkind = 'm'
                                   and oid::regclass::text LIKE 'monitoring.%';"""
        cur.execute(get_all_mv_SQL)
        rows = cur.fetchall()
        for row in rows:
            mvname = row[0]
            if mvname in ignores:
                continue
            logging.info('Refreshing', mvname, '...')
            cur.execute('REFRESH MATERIALIZED VIEW CONCURRENTLY %s;' % mvname)

        cur.close()
        self.CONN.commit()

    def upsert(self, table, tablename):

        # sanitise table name
        tablename = _quote(self.SCHEMA) + '.' + _quote(tablename)
        logging.debug('Tablename: %r', tablename)

        # sanitise field names
        it = iter(table)
        hdr = next(it)
        flds = list(map(text_type, hdr))
        colnames = [_quote(n) for n in flds]
        logging.debug('Column names: %r', colnames)

        # determine paramstyle and build placeholders string
        placeholders = _placeholders(self.CONN, colnames)
        logging.debug('Placeholders: %r', placeholders)

        # get a cursor
        cur = self.CONN.cursor()

        insertcolnames = ', '.join(colnames)
        insertquery = self.SQL_INSERT_QUERY % (tablename, insertcolnames, placeholders)
        logging.debug('Insert data via query %r' % insertquery)
        cur.executemany(insertquery, it)

        # finish up
        logging.debug('Close the cursor and commit')
        cur.close()
        self.CONN.commit()