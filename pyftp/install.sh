sudo apt-get install python-virtualenv build-essential python-dev

USR=pyftp
sudo useradd -m $USR
sudo -u postgres createuser --superuser $USR
#sudo -u postgres createdb $USR

VERSION=0.1
sudo mkdir /opt/pyftpd-${VERSION}
sudo virtualenv --system-site-packages /opt/pyftpd-${VERSION}/env
sudo cp *.py /opt/pyftpd-${VERSION}
sudo /opt/pyftpd-${VERSION}/env/bin/pip install -r requirements.txt
sudo chown $USR -R /opt/pyftpd-${VERSION}

sudo rm -R /opt/pyftpd
sudo ln -s /opt/pyftpd-${VERSION} /opt/pyftpd

sudo touch /var/log/pyftpd.log
sudo chown $USR /var/log/pyftpd.log

sudo cp ftp.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/ftp.service
sudo systemctl daemon-reload

sudo systemctl enable ftp.service
sudo systemctl start ftp.service
