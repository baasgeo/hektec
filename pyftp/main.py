#!/usr/bin/env python

"""
An RFC-4217 asynchronous FTPS server supporting both SSL and TLS.
Requires PyOpenSSL module (http://pypi.python.org/pypi/pyOpenSSL).
"""



import sys, getopt
from server import PythonFtpServer

def main(argv):
    # Server setting constants:
    ip = ''
    port = 2121
    production = False

    try:
        opts, args = getopt.getopt(argv, "hp:i:s", ["port=", "ip-address=", "production"])
    except getopt.GetoptError:
        print 'main.py -p <port> -i <ip-address> -t <test>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'main.py -p <port> -i <ip-address> -t <test>'
            sys.exit()
        elif opt in ("-p", "--port"):
            port = arg
        elif opt in ("-i", "--ip-address"):
            ip = arg
        elif opt in ("-s", "--production"):
            production = True

    print 'Startup parameters:', ip, port, production
    PythonFtpServer(ip, port, production).run()

if __name__ == "__main__":
    main(sys.argv[1:])
