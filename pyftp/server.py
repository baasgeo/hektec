import getpass
import logging
from os.path import expanduser, join, exists

from pyftpdlib.servers import FTPServer, MultiprocessFTPServer
from pyftpdlib.authorizers import DummyAuthorizer
from threading import Thread
from handler import MyHandler

class PythonFtpServer(Thread):
    def __init__(self, ip, port, mode):
        Thread.__init__(self)
        self.daemon = True

        print 'Running as user %s' % getpass.getuser()
        home = join('/home/', getpass.getuser())
        if not exists(home):
            home = expanduser('~')

        print 'FTP home directory is: %s' % home

        authorizer = DummyAuthorizer()
        authorizer.add_user('hektec', 'hektec1234', homedir=home, perm='elradfmwM')
        authorizer.add_anonymous(homedir=home)  # readonly permissions

        handler = MyHandler
        handler.authorizer = authorizer
        handler.masquerade_address = ip
        handler.passive_ports = range(60000, 65000) #ufw allow proto tcp from any to any port 60000:65000

        if mode:
            logging.basicConfig(filename='/var/log/pyftpd.log', level=logging.INFO)
        else:
            logging.basicConfig(level=logging.DEBUG)

        self.server = MultiprocessFTPServer(('', port), handler)
        # set a limit for connections
        self.server.max_cons = 256
        self.server.max_cons_per_ip = 5

    def stop(self):
        self.server.close_all()

    def run(self):
        Thread.run(self)
        # start ftp server
        self.server.serve_forever()