﻿-- authenticator can create new logins
GRANT INSERT ON TABLE basic_auth.users, basic_auth.tokens TO pgr_anon;
GRANT SELECT ON TABLE pg_authid, basic_auth.users TO pgr_anon;
GRANT EXECUTE ON FUNCTION
login(TEXT, TEXT),
signup(TEXT, TEXT)
TO pgr_authenticator;

-- pyftp can insert data
GRANT USAGE ON SCHEMA monitoring TO pyftp;
GRANT SELECT, INSERT, UPDATE ON TABLE monitoring.project TO pyftp;
GRANT SELECT, INSERT, UPDATE ON TABLE monitoring.sensor TO pyftp;
GRANT SELECT, INSERT, UPDATE ON TABLE monitoring.deformation TO pyftp;

-- viewer can view data
GRANT USAGE ON SCHEMA monitoring, basic_auth TO pgr_viewer;
GRANT SELECT ON TABLE pg_authid, basic_auth.users TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.project TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.sensor TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.deformation TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.project_user TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_delay TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_appliance TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_deformation TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_deformation_stats_1h TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_deformation_stats_2h TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_sensor TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_project TO pgr_viewer;
GRANT SELECT ON TABLE monitoring.calc_delay TO pgr_viewer;
GRANT EXECUTE ON FUNCTION monitoring.usersettings(TEXT, TEXT) TO pgr_viewer;

-- anon can login
GRANT USAGE ON SCHEMA monitoring, basic_auth TO pgr_anon;
GRANT SELECT ON TABLE pg_authid, basic_auth.users, monitoring.project_user TO pgr_anon;
GRANT EXECUTE ON FUNCTION monitoring.login(TEXT, TEXT) TO pgr_anon;


GRANT INSERT ON TABLE basic_auth.users, basic_auth.tokens TO anon;
GRANT SELECT ON TABLE pg_authid, basic_auth.users TO pgr_anon;
GRANT EXECUTE ON FUNCTION
login(TEXT, TEXT),
request_password_reset(TEXT),
reset_password(TEXT, UUID, TEXT),
signup(TEXT, TEXT)
TO pgr_anon;