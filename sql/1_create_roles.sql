-- the names "anon" and "authenticator" are configurable and not
-- sacred, we simply choose them for clarity
CREATE ROLE pgr_anon;
CREATE ROLE pgr_editor;
CREATE ROLE pgr_viewer;
CREATE ROLE pgr_authenticator NOINHERIT;
GRANT pgr_anon TO pgr_authenticator;
GRANT pgr_editor TO pgr_authenticator;
GRANT pgr_viewer TO pgr_authenticator;

GRANT pyftp TO pgr_authenticator;