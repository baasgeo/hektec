﻿SET ROLE pgr_authenticator;

CREATE OR REPLACE FUNCTION set_timestamp()
  RETURNS TRIGGER AS
$BODY$
BEGIN
  new := new #= hstore(tg_argv [0], 'now()');
  RETURN new;
END
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
GRANT EXECUTE ON FUNCTION set_timestamp() TO PUBLIC;

CREATE TRIGGER trg_set_mutatie
BEFORE UPDATE
  ON monitoring.project
FOR EACH ROW
EXECUTE PROCEDURE set_timestamp('modified');

CREATE TRIGGER trg_set_mutatie
BEFORE UPDATE
  ON monitoring.appliance
FOR EACH ROW
EXECUTE PROCEDURE set_timestamp('modified');

CREATE TRIGGER trg_set_mutatie
BEFORE UPDATE
  ON monitoring.boundary
FOR EACH ROW
EXECUTE PROCEDURE set_timestamp('modified');

CREATE TRIGGER trg_set_mutatie
BEFORE UPDATE
  ON monitoring.sensor
FOR EACH ROW
EXECUTE PROCEDURE set_timestamp('modified');

CREATE OR REPLACE FUNCTION monitoring.validate_deviation()
  RETURNS TRIGGER AS $$
DECLARE
  _deviation_check BOOLEAN;
  _notes           TEXT;
BEGIN
  SELECT (abs(sensor.east - new.east) > (appliance.deviation / 1000)
          OR (abs(sensor.north - new.north) > (appliance.deviation / 1000))
          OR (abs(sensor.elevation - new.elevation) > (appliance.deviation / 1000)))
  INTO _deviation_check
  FROM monitoring.sensor
    LEFT JOIN monitoring.appliance ON sensor.appliance_id = appliance.id
  WHERE sensor.id = new.sensor_id
  LIMIT 1;
  IF (_deviation_check = TRUE)
  THEN
    SELECT concat('Uitgesloten i.v.m. overschrijding tolerantie van ', deviation, ' mm')
    INTO _notes
    FROM monitoring.sensor
      LEFT JOIN monitoring.appliance ON sensor.appliance_id = appliance.id
    WHERE sensor.id = new.sensor_id
    LIMIT 1;
    new.omit = _deviation_check;
    new.notes = _notes;
  END IF;
  RETURN new;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS validate_deviation_trigger
ON monitoring.deformation;
CREATE TRIGGER validate_deviation_trigger
BEFORE INSERT ON monitoring.deformation
FOR EACH ROW EXECUTE PROCEDURE monitoring.validate_deviation();
