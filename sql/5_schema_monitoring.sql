﻿--https://launchbylunch.com/posts/2014/Feb/16/sql-naming-conventions/

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS hstore;

SET ROLE pgr_authenticator;
DROP SCHEMA IF EXISTS monitoring CASCADE;
CREATE SCHEMA monitoring;

CREATE TABLE monitoring.enum_kind (
  value TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE monitoring.enum_language (
  value TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE monitoring.project (
  id          SERIAL PRIMARY KEY,
  identity    INTEGER,
  geom        GEOMETRY(MULTIPOLYGON, 28992),
  created     TIMESTAMP NOT NULL                                                                                 DEFAULT NOW(),
  modified    TIMESTAMP,
  title       TEXT,
  description TEXT,
  notes       TEXT,
  language    TEXT      NOT NULL REFERENCES monitoring.enum_language (value) ON UPDATE CASCADE ON DELETE CASCADE DEFAULT 'nl',
  active      BOOLEAN   NOT NULL                                                                                 DEFAULT TRUE
);
-- Project identity must be unique but user is allowed to rename a project
CREATE UNIQUE INDEX project_identity_uidx
  ON monitoring.project (identity);
CREATE INDEX project_geom_gist
  ON monitoring.project USING GIST (geom);

CREATE TABLE monitoring.project_user (
  project_id INTEGER NOT NULL REFERENCES monitoring.project (id) ON UPDATE NO ACTION ON DELETE CASCADE,
  user_id    INTEGER NOT NULL REFERENCES basic_auth.users (id) ON UPDATE NO ACTION ON DELETE CASCADE,
  PRIMARY KEY (project_id, user_id)
);

CREATE TABLE monitoring.appliance (
  id          SERIAL PRIMARY KEY,
  identity    INTEGER   NOT NULL,
  geom        GEOMETRY(POINT, 28992),
  created     TIMESTAMP NOT NULL                                                                                 DEFAULT NOW(),
  modified    TIMESTAMP,
  project_id  INTEGER   NOT NULL REFERENCES monitoring.project (id) ON DELETE CASCADE,
  title       TEXT,
  description TEXT,
  notes       TEXT,
  kind        TEXT      NOT NULL REFERENCES monitoring.enum_kind (value) ON UPDATE CASCADE ON DELETE CASCADE,
  deviation   NUMERIC                                                                                            DEFAULT 150,
  effectual   TIME WITHOUT TIME ZONE                                                                             DEFAULT '00:59:00',
  active      BOOLEAN   NOT NULL                                                                                 DEFAULT TRUE
);
COMMENT ON TABLE monitoring.appliance IS 'Equipment designed to perform the specific measuring task';
COMMENT ON COLUMN monitoring.appliance.deviation IS 'Allowed deviation in millimeters';
COMMENT ON COLUMN monitoring.appliance.effectual IS 'Allowed time in minutes before appliance is flagged as offline';
CREATE UNIQUE INDEX sensor_appliance_uidx
  ON monitoring.appliance (project_id, identity);
CREATE INDEX appliance_geom_gist
  ON monitoring.appliance USING GIST (geom);

CREATE TABLE monitoring.boundary (
  id               SERIAL PRIMARY KEY,
  created          TIMESTAMP NOT NULL                                                                                 DEFAULT NOW(),
  modified         TIMESTAMP,
  project_id       INTEGER   NOT NULL REFERENCES monitoring.project (id) ON DELETE CASCADE,
  title            TEXT,
  description      TEXT,
  notes            TEXT,
  frequency        TIME,
  alert_east       NUMERIC,
  alert_north      NUMERIC,
  alert_elevation  NUMERIC,
  danger_east      NUMERIC,
  danger_north     NUMERIC,
  danger_elevation NUMERIC
);
COMMENT ON COLUMN monitoring.boundary.frequency IS 'Interval in minutes';

CREATE TABLE monitoring.sensor (
  id           SERIAL PRIMARY KEY,
  identity     INTEGER   NOT NULL,
  geom         GEOMETRY(POINTZ, 28992),
  created      TIMESTAMP NOT NULL DEFAULT NOW(),
  modified     TIMESTAMP,
  project_id   INTEGER   NOT NULL REFERENCES monitoring.project (id) ON DELETE CASCADE,
  appliance_id INTEGER REFERENCES monitoring.appliance (id) ON DELETE CASCADE,
  boundary_id  INTEGER REFERENCES monitoring.boundary (id) ON DELETE CASCADE,
  east         NUMERIC,
  north        NUMERIC,
  elevation    NUMERIC,
  title        TEXT,
  code         TEXT,
  description  TEXT,
  notes        TEXT,
  active       BOOLEAN   NOT NULL DEFAULT TRUE
);
COMMENT ON COLUMN monitoring.sensor.identity IS 'Lookup value for survey station';
COMMENT ON COLUMN monitoring.sensor.geom IS 'Note: geometry includes Z value';
CREATE UNIQUE INDEX sensor_identity_uidx
  ON monitoring.sensor (project_id, identity);
CREATE INDEX sensor_geom_gist
  ON monitoring.sensor USING GIST (geom);

CREATE TABLE monitoring.deformation (
  sensor_id INTEGER   NOT NULL REFERENCES monitoring.sensor (id) ON UPDATE CASCADE ON DELETE CASCADE,
  index     INTEGER,
  run       TIMESTAMP NOT NULL DEFAULT NOW(),
  duration  TIME,
  east      NUMERIC,
  north     NUMERIC,
  elevation NUMERIC,
  filename  TEXT,
  omit      BOOLEAN   NOT NULL DEFAULT FALSE,
  notes     TEXT,
  PRIMARY KEY (sensor_id, run)
);
CREATE INDEX deformation_run_idx
  ON monitoring.deformation (run);
CREATE INDEX deformation_sensor_id_idx
  ON monitoring.deformation (sensor_id);

CREATE TABLE monitoring.vibration
(
  sensor_id INTEGER                     NOT NULL REFERENCES monitoring.sensor (id) ON UPDATE CASCADE ON DELETE CASCADE,
  index     INTEGER,
  run       TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  duration  TIME WITHOUT TIME ZONE,
  value     NUMERIC,
  filename  TEXT,
  omit      BOOLEAN                     NOT NULL DEFAULT FALSE,
  PRIMARY KEY (sensor_id, index)
);
CREATE INDEX vibration_run_idx
  ON monitoring.vibration (run);


CREATE OR REPLACE VIEW monitoring.calc_deformation AS
  SELECT
    deformation.*,
    lag(deformation.index)
    OVER w                                                                 AS index_prev_id,
    lead(deformation.index)
    OVER w                                                                 AS index_next_id,
    sensor.project_id,
    sensor.boundary_id,
    sensor.appliance_id,
    sensor.identity,
    sensor.east                                                            AS ref_east,
    sensor.north                                                           AS ref_north,
    sensor.elevation                                                       AS ref_elevation,
    round((sensor.east - deformation.east) * 1000 :: NUMERIC, 1)           AS delta_ref_east,
    round((sensor.north - deformation.north) * 1000 :: NUMERIC, 1)         AS delta_ref_north,
    round((sensor.elevation - deformation.elevation) * 1000 :: NUMERIC, 1) AS delta_ref_elevation,
    round((lag(deformation.east)
           OVER w - deformation.east) * 1000 :: NUMERIC, 1)                AS delta_prev_east,
    round((lag(deformation.north)
           OVER w - deformation.north) * 1000 :: NUMERIC, 1)               AS delta_prev_north,
    round((lag(deformation.elevation)
           OVER w - deformation.elevation) * 1000 :: NUMERIC, 1)           AS delta_prev_elevation
  FROM monitoring.sensor,
    monitoring.deformation
  WHERE sensor.id = deformation.sensor_id AND deformation.omit = FALSE
  WINDOW w AS (
    PARTITION BY deformation.sensor_id
    ORDER BY deformation.sensor_id, deformation.run );

CREATE OR REPLACE VIEW monitoring.calc_deformation_stats_now AS
  SELECT
    calc_deformation.sensor_id,
    count(calc_deformation.run)                              AS measures,
    avg(calc_deformation.duration :: INTERVAL)               AS duration,
    min(calc_deformation.run)                                AS run_first,
    max(calc_deformation.run)                                AS run_last,
    round(avg(calc_deformation.delta_ref_east), 1)           AS avg_delta_ref_east,
    round(avg(calc_deformation.delta_ref_north), 1)          AS avg_delta_ref_north,
    round(avg(calc_deformation.delta_ref_elevation), 1)      AS avg_delta_ref_elevation,
    round(avg(calc_deformation.delta_prev_east), 1)          AS avg_delta_prev_east,
    round(avg(calc_deformation.delta_prev_north), 1)         AS avg_delta_prev_north,
    round(avg(calc_deformation.delta_prev_elevation), 1)     AS avg_delta_prev_elevation,
    round(max(calc_deformation.delta_ref_east), 1)           AS max_delta_ref_east,
    round(max(calc_deformation.delta_ref_north), 1)          AS max_delta_ref_north,
    round(max(calc_deformation.delta_ref_elevation), 1)      AS max_delta_ref_elevation,
    round(max(calc_deformation.delta_prev_east), 1)          AS max_delta_prev_east,
    round(max(calc_deformation.delta_prev_north), 1)         AS max_delta_prev_north,
    round(max(calc_deformation.delta_prev_elevation), 1)     AS max_delta_prev_elevation,
    round(abs(avg(calc_deformation.delta_ref_east)), 1)      AS abs_east,
    round(abs(avg(calc_deformation.delta_ref_north)), 1)     AS abs_north,
    round(abs(avg(calc_deformation.delta_ref_elevation)), 1) AS abs_elevation
  FROM monitoring.calc_deformation
  WHERE run >= (now() - INTERVAL '1 hour')
  GROUP BY calc_deformation.sensor_id;

CREATE OR REPLACE VIEW monitoring.calc_deformation_stats_1h AS
  SELECT
    sensor_id,
    count(run)    AS                        measures,
    avg(duration) AS                        duration,
    min(run)      AS                        run_first,
    max(run)      AS                        run_last,
    round(avg(delta_ref_east), 1)           avg_delta_ref_east,
    round(avg(delta_ref_north), 1)          avg_delta_ref_north,
    round(avg(delta_ref_elevation), 1)      avg_delta_ref_elevation,
    round(avg(delta_prev_east), 1)          avg_delta_prev_east,
    round(avg(delta_prev_north), 1)         avg_delta_prev_north,
    round(avg(delta_prev_elevation), 1)     avg_delta_prev_elevation,
    round(max(delta_ref_east), 1)           max_delta_ref_east,
    round(max(delta_ref_north), 1)          max_delta_ref_north,
    round(max(delta_ref_elevation), 1)      max_delta_ref_elevation,
    round(max(delta_prev_east), 1)          max_delta_prev_east,
    round(max(delta_prev_north), 1)         max_delta_prev_north,
    round(max(delta_prev_elevation), 1)     max_delta_prev_elevation,
    round(abs(avg(delta_ref_east)), 1)      abs_east,
    round(abs(avg(delta_ref_north)), 1)     abs_north,
    round(abs(avg(delta_ref_elevation)), 1) abs_elevation
  FROM monitoring.calc_deformation
  WHERE run >= (SELECT max(run)
                FROM monitoring.deformation) - INTERVAL '1 hour'
  GROUP BY sensor_id;

CREATE OR REPLACE VIEW monitoring.calc_deformation_stats_2h AS
  SELECT
    sensor_id,
    count(run)    AS                        measures,
    avg(duration) AS                        duration,
    min(run)      AS                        run_first,
    max(run)      AS                        run_last,
    round(avg(delta_ref_east), 1)           avg_delta_ref_east,
    round(avg(delta_ref_north), 1)          avg_delta_ref_north,
    round(avg(delta_ref_elevation), 1)      avg_delta_ref_elevation,
    round(avg(delta_prev_east), 1)          avg_delta_prev_east,
    round(avg(delta_prev_north), 1)         avg_delta_prev_north,
    round(avg(delta_prev_elevation), 1)     avg_delta_prev_elevation,
    round(max(delta_ref_east), 1)           max_delta_ref_east,
    round(max(delta_ref_north), 1)          max_delta_ref_north,
    round(max(delta_ref_elevation), 1)      max_delta_ref_elevation,
    round(max(delta_prev_east), 1)          max_delta_prev_east,
    round(max(delta_prev_north), 1)         max_delta_prev_north,
    round(max(delta_prev_elevation), 1)     max_delta_prev_elevation,
    round(abs(avg(delta_ref_east)), 1)      abs_east,
    round(abs(avg(delta_ref_north)), 1)     abs_north,
    round(abs(avg(delta_ref_elevation)), 1) abs_elevation
  FROM monitoring.calc_deformation
  WHERE run >= (SELECT max(run)
                FROM monitoring.deformation) - INTERVAL '2 hour'
  GROUP BY sensor_id;

CREATE OR REPLACE VIEW monitoring.users AS
  SELECT
    actual.role,
    '***' :: TEXT AS pass,
    actual.email,
    actual.verified,
    actual.userdata
  FROM basic_auth.users AS actual,
    (SELECT rolname
     FROM pg_authid
     WHERE pg_has_role(current_user, oid, 'member')
    ) AS member_of
  WHERE actual.role = member_of.rolname;
-- can also add restriction that current_setting('postgrest.claims.email')
-- is equal to email so that user can only see themselves

INSERT INTO monitoring.enum_kind (value) VALUES ('deformation');
INSERT INTO monitoring.enum_kind (value) VALUES ('vibration');
INSERT INTO monitoring.enum_kind (value) VALUES ('noise');
INSERT INTO monitoring.enum_language (value) VALUES ('nl');
INSERT INTO monitoring.enum_language (value) VALUES ('en');

-- Login functions for api
CREATE OR REPLACE FUNCTION
  monitoring.signup(email TEXT, pass TEXT)
  RETURNS VOID
AS $$
INSERT INTO basic_auth.users (email, pass, role) VALUES
  (signup.email, signup.pass, 'pgr_viewer');
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
  monitoring.login(email TEXT, pass TEXT)
  RETURNS basic_auth.JWT_TOKEN
LANGUAGE plpgsql
AS $$
DECLARE
  _role     NAME;
  _verified BOOLEAN;
  _email    TEXT;
  _projects INTEGER [];
  result    basic_auth.JWT_TOKEN;
BEGIN
  -- lower case email
  email := lower(email);
  -- check email and password
  SELECT basic_auth.user_role(email, pass)
  INTO _role;
  IF _role IS NULL
  THEN
    RAISE invalid_password
    USING MESSAGE = 'invalid user or password';
  END IF;
  -- check verified flag whether users
  -- have validated their emails
  _email := email;
  SELECT verified
  FROM basic_auth.users AS u
  WHERE u.email = _email
  LIMIT 1
  INTO _verified;
  IF NOT _verified
  THEN
    RAISE invalid_authorization_specification
    USING MESSAGE = 'user is not verified';
  END IF;
  -- check if user is assigned to a project
  SELECT array_agg(project_id) AS projects
  FROM monitoring.project_user
  WHERE user_id = (
    SELECT id
    FROM basic_auth.users AS u
    WHERE u.email = _email
    LIMIT 1
  )
  INTO _projects;
  IF count(_projects) < 1
  THEN
    RAISE invalid_authorization_specification
    USING MESSAGE = 'user is not assigned to a project';
  END IF;

  SELECT basic_auth.sign(
             row_to_json(r), current_setting('app.jwt_secret')
         ) AS token
  FROM (
         SELECT
           _role                                              AS role,
           _projects                                          AS projects,
           login.email                                        AS email,
           extract(EPOCH FROM now()) :: INTEGER + 8 * 60 * 60 AS exp,
           extract(EPOCH FROM now()) :: INTEGER               AS iat
       ) r
  INTO result;
  RETURN result;
END;
$$;
