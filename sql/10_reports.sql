SET ROLE pgr_authenticator;

CREATE OR REPLACE VIEW monitoring.report_delay AS
  SELECT
    project.identity AS                               project,
    sensor.identity  AS                               sensor,
    appliance.title  AS                               appliance,
    appliance.id     AS                               appliance_id,
    deformation.run,
    boundary.frequency,
    (deformation.run <= (now() - boundary.frequency)) delay
  FROM monitoring.sensor
    LEFT JOIN (SELECT
                 sensor_id,
                 max(run) run
               FROM monitoring.deformation
               GROUP BY sensor_id) AS deformation ON
                                                    deformation.sensor_id = sensor.id
    LEFT JOIN monitoring.boundary ON sensor.boundary_id = boundary.id
    LEFT JOIN monitoring.appliance ON sensor.appliance_id = appliance.id
    LEFT JOIN monitoring.project ON sensor.project_id = project.id
  WHERE project.active = TRUE
  ORDER BY project, appliance_id, sensor;

CREATE OR REPLACE VIEW monitoring.report_exceedance AS
  SELECT
    project.identity                                 AS project,
    sensor.identity                                  AS sensor,
    appliance.title                                  AS appliance,
    boundary.alert_east,
    boundary.alert_north,
    boundary.alert_elevation,
    boundary.danger_east,
    boundary.danger_north,
    boundary.danger_elevation,
    stats.run_first,
    stats.run_last,
    stats.avg_delta_ref_east,
    stats.avg_delta_ref_north,
    stats.avg_delta_ref_elevation,
    stats.abs_east >= boundary.alert_east            AS alert_east_exceedance,
    stats.abs_north >= boundary.alert_north          AS alert_north_exceedance,
    stats.abs_elevation >= boundary.alert_elevation  AS alert_elevation_exceedance,
    stats.abs_east >= boundary.danger_east           AS danger_east_exceedance,
    stats.abs_north >= boundary.danger_north         AS danger_north_exceedance,
    stats.abs_elevation >= boundary.danger_elevation AS danger_elevation_exceedance
  FROM monitoring.sensor
    LEFT JOIN monitoring.calc_deformation_stats_now stats ON stats.sensor_id = sensor.id
    LEFT JOIN monitoring.boundary ON sensor.boundary_id = boundary.id
    LEFT JOIN monitoring.appliance ON sensor.appliance_id = appliance.id
    LEFT JOIN monitoring.project ON sensor.project_id = project.id
  WHERE project.active = TRUE;

CREATE OR REPLACE VIEW monitoring.report_appliance AS
  SELECT
    appliance.identity appliance,
    appliance.title,
    project.identity   project,
    delay.sensor_count,
    delay.nodelay_count,
    delay.delay_count,
    delay.offline
  FROM monitoring.appliance
    LEFT JOIN (SELECT
                 report_delay.appliance_id,
                 count(*)                                             AS sensor_count,
                 count(NULLIF(report_delay.delay, TRUE))              AS nodelay_count,
                 count(NULLIF(report_delay.delay, FALSE))             AS delay_count,
                 count(NULLIF(report_delay.delay, FALSE)) >= count(*) AS offline
               FROM monitoring.report_delay
               GROUP BY report_delay.appliance_id) delay ON appliance.id = delay.appliance_id
    LEFT JOIN monitoring.project ON appliance.project_id = project.id
  WHERE project.active = TRUE;

CREATE OR REPLACE VIEW monitoring.report_deformation_week AS
  SELECT
    appliance.title                                       AS appliance,
    boundary.alert_east,
    boundary.alert_north,
    boundary.alert_elevation,
    boundary.danger_east,
    boundary.danger_north,
    boundary.danger_elevation,
    calc_deformation.*,
    abs(delta_ref_east) >= boundary.alert_east            AS alert_east_exceedance,
    abs(delta_ref_north) >= boundary.alert_north          AS alert_north_exceedance,
    abs(delta_ref_elevation) >= boundary.alert_elevation  AS alert_elevation_exceedance,
    abs(delta_ref_east) >= boundary.danger_east           AS danger_east_exceedance,
    abs(delta_ref_north) >= boundary.danger_north         AS danger_north_exceedance,
    abs(delta_ref_elevation) >= boundary.danger_elevation AS danger_elevation_exceedance
  FROM monitoring.calc_deformation
    LEFT JOIN monitoring.boundary ON boundary_id = boundary.id
    LEFT JOIN monitoring.appliance ON appliance_id = appliance.id
    LEFT JOIN monitoring.project ON calc_deformation.project_id = project.id
  WHERE date_part('week' :: TEXT, run) = date_part('week' :: TEXT, now())
        AND project.active = TRUE
  ORDER BY appliance, run;