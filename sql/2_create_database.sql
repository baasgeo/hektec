﻿-- Database maken
CREATE DATABASE hektec
  WITH OWNER = pgr_authenticator
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.utf-8'
       LC_CTYPE = 'en_US.utf-8'
       CONNECTION LIMIT = -1
       TEMPLATE=template0;

--REVOKE ALL ON DATABASE oiv_prod FROM public;

COMMENT ON DATABASE hektec
  IS 'Hektec measurements database';

-- run this once
ALTER DATABASE hektec SET "app.jwt_secret" TO '!!hektec!!';