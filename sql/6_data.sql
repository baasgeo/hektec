﻿-- Opzoektabellen vullen

-- first users
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('test@baasgeo.com'), 'welkom', 'pgr_viewer', TRUE);

INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('P.IJnsen@vanthekgroep.nl'), 'welkom', 'pgr_viewer', TRUE);

INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('N.Scheide@hektec.nl'), 'welkom', 'pgr_viewer', TRUE);

INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('b.baas@baasgeo.com'), 'bartje', 'pgr_authenticator', TRUE);

-- project users
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('hansvanpruissen@halteren.com'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('riekusbakker@halteren.com'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('vincent@hvsteenwijk.nl'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('mark@hvsteenwijk.nl'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('r.verbeek@utrecht.nl'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('g.heenck@utrecht.nl'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('eustonjohanna@gmail.com'), 'Hekm0n', 'pgr_viewer', TRUE);
INSERT INTO basic_auth.users (email, pass, role, verified) VALUES
  (lower('barry@hvsteenwijk.nl'), 'Hekm0n', 'pgr_viewer', TRUE);

INSERT INTO monitoring.project_user (project_id, user_id) VALUES (6, 17);