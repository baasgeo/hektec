WITH t AS (
    SELECT
      sensor.id                                                        rowid,
      deformation.run                                                  rowrun,
      deviation_mm                                                     rowdeviation,
      (abs(sensor.east - deformation.east) > deviation
       OR (abs(sensor.north - deformation.north) > deviation)
       OR (abs(sensor.elevation - deformation.elevation) > deviation)) newomit
    FROM monitoring.deformation,
      (SELECT
         sensor.id,
         sensor.east,
         sensor.north,
         sensor.elevation,
         appliance.deviation deviation_mm,
         (deviation / 1000)  deviation
       FROM monitoring.sensor
         LEFT JOIN monitoring.appliance ON appliance_id = appliance.id) sensor
    WHERE sensor.id = deformation.sensor_id
)
UPDATE monitoring.deformation
SET omit = t.newomit, notes = concat('Uitgesloten i.v.m. overschrijding tolerantie van ', t.rowdeviation, ' mm')
FROM t
WHERE sensor_id = t.rowid AND run = t.rowrun AND t.newomit = TRUE