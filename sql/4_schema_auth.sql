﻿CREATE EXTENSION IF NOT EXISTS pgcrypto;

SET ROLE pgr_authenticator;
-- We put things inside the basic_auth schema to hide
-- them from public view. Certain public procs/views will
-- refer to helpers and tables inside.
DROP SCHEMA IF EXISTS basic_auth CASCADE;
CREATE SCHEMA IF NOT EXISTS basic_auth;

CREATE TABLE IF NOT EXISTS
  basic_auth.users (
  id       SERIAL PRIMARY KEY,
  email    TEXT UNIQUE CHECK ( email ~* '^.+@.+\..+$' AND email = lower(email)),
  pass     TEXT      NOT NULL CHECK (length(pass) < 512),
  role     NAME      NOT NULL CHECK (length(role) < 512),
  verified BOOLEAN   NOT NULL DEFAULT FALSE,
  created  TIMESTAMP NOT NULL DEFAULT NOW(),
  modified TIMESTAMP,
  userdata JSON
  -- If you like add more columns, or a json column
);

CREATE OR REPLACE FUNCTION
  basic_auth.check_role_exists()
  RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
  IF NOT exists(SELECT 1
                FROM pg_roles AS r
                WHERE r.rolname = new.role)
  THEN
    RAISE foreign_key_violation
    USING MESSAGE =
      'unknown database role: ' || new.role;
    RETURN NULL;
  END IF;
  RETURN new;
END
$$;

DROP TRIGGER IF EXISTS ensure_user_role_exists
ON basic_auth.users;
CREATE CONSTRAINT TRIGGER ensure_user_role_exists
AFTER INSERT OR UPDATE ON basic_auth.users
FOR EACH ROW
EXECUTE PROCEDURE basic_auth.check_role_exists();

CREATE OR REPLACE FUNCTION
  basic_auth.encrypt_pass()
  RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
  IF tg_op = 'INSERT' OR new.pass <> old.pass
  THEN
    new.pass = crypt(new.pass, gen_salt('bf'));
  END IF;
  RETURN new;
END
$$;

DROP TRIGGER IF EXISTS encrypt_pass
ON basic_auth.users;
CREATE TRIGGER encrypt_pass
BEFORE INSERT OR UPDATE ON basic_auth.users
FOR EACH ROW
EXECUTE PROCEDURE basic_auth.encrypt_pass();

CREATE OR REPLACE FUNCTION
  basic_auth.user_role(email TEXT, pass TEXT)
  RETURNS NAME
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN (
    SELECT role
    FROM basic_auth.users
    WHERE users.email = user_role.email
          AND users.pass = crypt(user_role.pass, users.pass)
  );
END;
$$;

-- jwt
DROP TYPE IF EXISTS basic_auth.jwt_claims CASCADE;
CREATE TYPE basic_auth.jwt_claims AS (role TEXT, email TEXT, exp INTEGER);
DROP TYPE IF EXISTS basic_auth.jwt_token CASCADE;
CREATE TYPE basic_auth.jwt_token AS (
  token TEXT
);
CREATE OR REPLACE FUNCTION basic_auth.url_encode(data BYTEA)
  RETURNS TEXT LANGUAGE SQL AS $$
SELECT translate(encode(data, 'base64'), E'+/=\n', '-_');
$$;


CREATE OR REPLACE FUNCTION basic_auth.url_decode(data TEXT)
  RETURNS BYTEA LANGUAGE SQL AS $$
WITH t AS (SELECT translate(data, '-_', '+/')),
    rem AS (SELECT length((SELECT *
                           FROM t)) % 4) -- compute padding size
SELECT decode(
    (SELECT *
     FROM t) ||
    CASE WHEN (SELECT *
               FROM rem) > 0
      THEN repeat('=', (4 - (SELECT *
                             FROM rem)))
    ELSE '' END,
    'base64');
$$;


CREATE OR REPLACE FUNCTION basic_auth.algorithm_sign(signables TEXT, secret TEXT, algorithm TEXT)
  RETURNS TEXT LANGUAGE SQL AS $$
WITH
    alg AS (
      SELECT CASE
             WHEN algorithm = 'HS256'
               THEN 'sha256'
             WHEN algorithm = 'HS384'
               THEN 'sha384'
             WHEN algorithm = 'HS512'
               THEN 'sha512'
             ELSE '' END) -- hmac throws error
SELECT basic_auth.url_encode(hmac(signables, secret, (SELECT *
                                                      FROM alg)));
$$;


CREATE OR REPLACE FUNCTION basic_auth.sign(payload JSON, secret TEXT, algorithm TEXT DEFAULT 'HS256')
  RETURNS TEXT LANGUAGE SQL AS $$
WITH
    header AS (
      SELECT basic_auth.url_encode(convert_to('{"alg":"' || algorithm || '","typ":"JWT"}', 'utf8'))
  ),
    payload AS (
      SELECT basic_auth.url_encode(convert_to(payload :: TEXT, 'utf8'))
  ),
    signables AS (
      SELECT (SELECT *
              FROM header) || '.' || (SELECT *
                                      FROM payload)
  )
SELECT (SELECT *
        FROM signables)
       || '.' ||
       basic_auth.algorithm_sign((SELECT *
                                  FROM signables), secret, algorithm);
$$;


CREATE OR REPLACE FUNCTION basic_auth.verify(token TEXT, secret TEXT, algorithm TEXT DEFAULT 'HS256')
  RETURNS TABLE(header JSON, payload JSON, valid BOOLEAN) LANGUAGE SQL AS $$
SELECT
  convert_from(basic_auth.url_decode(r [1]), 'utf8') :: JSON                  AS header,
  convert_from(basic_auth.url_decode(r [2]), 'utf8') :: JSON                  AS payload,
  r [3] = basic_auth.algorithm_sign(r [1] || '.' || r [2], secret, algorithm) AS valid
FROM regexp_split_to_array(token, '\.') r;
$$;