SET ROLE pgr_authenticator;

CREATE OR REPLACE VIEW monitoring.webapp_appliance AS
  SELECT
    appliance.*,
    ((delay.run + appliance.effectual) < now()) offline,
    project.identity AS project,
    delay.run,
    delay.sensor_count,
    delay.nodelay_count,
    delay.delay_count
  FROM monitoring.appliance
    LEFT JOIN (SELECT
                 calc_delay.appliance_id,
                 max(run)                                           AS run,
                 count(*)                                           AS sensor_count,
                 count(NULLIF(calc_delay.delay, TRUE))              AS nodelay_count,
                 count(NULLIF(calc_delay.delay, FALSE))             AS delay_count
               FROM monitoring.calc_delay
               GROUP BY calc_delay.appliance_id) delay ON appliance.id = delay.appliance_id
    LEFT JOIN monitoring.project ON appliance.project_id = project.id;

GRANT SELECT ON TABLE monitoring.webapp_appliance TO pgr_viewer;

CREATE OR REPLACE VIEW monitoring.webapp_delay AS
  SELECT
    project.identity                                            AS project,
    sensor.identity                                             AS sensor,
    appliance.id                                                AS appliance_id,
    deformation.run,
    boundary.frequency,
    deformation.run <= (now() - boundary.frequency :: INTERVAL) AS delay
  FROM monitoring.sensor
    LEFT JOIN (SELECT
                 deformation_1.sensor_id,
                 max(deformation_1.run) AS run
               FROM monitoring.deformation deformation_1
               GROUP BY deformation_1.sensor_id) deformation ON deformation.sensor_id = sensor.id
    LEFT JOIN monitoring.boundary ON sensor.boundary_id = boundary.id
    LEFT JOIN monitoring.appliance ON sensor.appliance_id = appliance.id
    LEFT JOIN monitoring.project ON sensor.project_id = project.id;

GRANT SELECT ON TABLE monitoring.webapp_delay TO pgr_viewer;

CREATE OR REPLACE VIEW monitoring.webapp_deformation AS
  SELECT *
  FROM monitoring.calc_deformation
  ORDER BY calc_deformation.sensor_id, calc_deformation.run DESC;

GRANT SELECT ON TABLE monitoring.webapp_deformation TO pgr_viewer;

CREATE OR REPLACE VIEW monitoring.webapp_sensor AS
  SELECT
    sensor.*,
    appliance.active appliance_active,
    boundary.frequency,
    boundary.alert_east,
    boundary.alert_north,
    boundary.alert_elevation,
    boundary.danger_east,
    boundary.danger_north,
    boundary.danger_elevation,
    stats.*
  FROM monitoring.sensor
    LEFT JOIN monitoring.calc_deformation_stats_2h stats ON stats.sensor_id = sensor.id
    LEFT JOIN monitoring.boundary ON boundary_id = boundary.id
    LEFT JOIN monitoring.appliance ON appliance_id = appliance.id
  WHERE sensor.active != FALSE
  ORDER BY abs_elevation DESC, abs_north DESC, abs_east DESC;

GRANT SELECT ON TABLE monitoring.webapp_sensor TO pgr_viewer;

CREATE OR REPLACE VIEW monitoring.webapp_project AS
  SELECT
    project.*,
    appliance.appliance_count,
    sensor.sensor_count,
    deformation.deformation_count,
    deformation.deformation_run_max AS run_latest,
    deformation.deformation_run_max
  FROM monitoring.project
    LEFT JOIN (SELECT
                 appliance_1.project_id,
                 count(*) AS appliance_count
               FROM monitoring.appliance appliance_1
               GROUP BY appliance_1.project_id) appliance ON project.id = appliance.project_id
    LEFT JOIN (SELECT
                 sensor_1.project_id,
                 count(*) AS sensor_count
               FROM monitoring.sensor sensor_1
               GROUP BY sensor_1.project_id) sensor ON project.id = sensor.project_id
    LEFT JOIN (SELECT
                 sensor_1.project_id,
                 count(deformation_1.*) AS deformation_count,
                 max(deformation_1.run) AS deformation_run_max
               FROM monitoring.sensor sensor_1
                 LEFT JOIN monitoring.deformation deformation_1 ON sensor_1.id = deformation_1.sensor_id
               GROUP BY sensor_1.project_id) deformation ON project.id = deformation.project_id;

GRANT SELECT ON TABLE monitoring.webapp_project TO pgr_viewer;