import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Http, RequestOptions, HttpModule} from "@angular/http";
import {AuthConfig, AuthHttp} from "angular2-jwt";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {AppConfig, APP_CONFIG} from "./app.config";
import {AppService} from "./app.service";
import {AuthModule} from "./shared/auth/auth.module";

import {SIDEBAR_TOGGLE_DIRECTIVES} from './shared/sidebar.directive';
import {BreadcrumbsComponent} from './shared/breadcrumb.component';

// Routing Module
import {AppRoutingModule} from './app.routing';

//Layouts
import {FullLayoutComponent} from './layouts/full-layout.component';
import {SimpleLayoutComponent} from './layouts/simple-layout.component';

//Shared services
import {ProjectService, SensorService, DeformationService} from "./shared/services/";
import {ApplianceService} from "./shared/services/appliance.service";

// Authentication service
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    //tokenName: 'token',
    tokenGetter: (() => localStorage.getItem('token')),
    globalHeaders: [{'Content-Type': 'application/json'}],
  }), http, options);
}

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AuthModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES
  ],
  providers: [
    {provide: APP_CONFIG, useValue: AppConfig},
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    AppService,
    ProjectService,
    SensorService,
    ApplianceService,
    DeformationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
