import {Injectable} from "@angular/core";

@Injectable()
export class AppService {
  // Use this for parent-child communication with route-outlet
  // http://stackoverflow.com/questions/41451375/passing-data-into-router-outlet-child-components-angular-2
  projectId: number;
  applianceId: number;
}
