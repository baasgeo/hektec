export class Sensor {
  id: number;
  identity: number;
  title: string;
  created: Date;
  modified: Date;
  description: string;
  notes: string;
  language: string;
  active: boolean;
}
