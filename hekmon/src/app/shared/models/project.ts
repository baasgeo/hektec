export class Project {
  id: number;
  identity: number;
  title: string;
  created: Date;
  modified: Date;
  description: string;
  notes: string;
  language: string;
  active: boolean;
  sensor_count: number;
  deformation_count: number;
  appliance_count: number;
  deformation_run_max: Date;
  run_latest: number;
}
