import {Inject, Injectable} from "@angular/core";
import {Response, Headers, RequestOptions, URLSearchParams} from "@angular/http";

import {AuthHttp} from "angular2-jwt";
import {Sensor} from "../models/";

import {APP_CONFIG} from "../../app.config";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ApplianceService {
  viewUrl: string;
  writeUrl: string;
  primary_key: string = 'id';
  view: string = 'webapp_appliance';
  table: string = 'appliance';
  interval: number = 5 * 60 * 1000;

  private subscription;
  private options = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});

  constructor(@Inject(APP_CONFIG) private config,
              private authHttp: AuthHttp) {
    this.viewUrl = `${config.apiEndpoint}${this.view}`;
    this.writeUrl = `${config.apiEndpoint}${this.table}`;
  }

  get(identifier: number, onNext: (data: any) => void) {
    this.authHttp
      .get(`${this.viewUrl}/${identifier}`)
      .map(response => <any>response.json()[0])
      .retry(2)
      .subscribe(onNext,
        error =>
          error => console.log(`An error occurred when requesting ${this.view}`, error)
      );
  }

  getByProjectId(project: number, onNext: (data: any[]) => void) {
    let searchParam = new URLSearchParams();
    searchParam.set('project_id', `eq.${project}`);
    this.options.search = searchParam;
    this.authHttp
      .get(this.viewUrl, this.options)
      .map(response => <any[]>response.json())
      .retry(2)
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  add(data:any, onNext: (data: any) => void) {
    Object.keys(data).forEach((key) => (data[key] == '') && delete data[key]);
    this.authHttp.post(`${this.writeUrl}`, data)
      .map((response: Response) => <any>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  remove(id:string, onNext: (data: any) => void) {
    let searchParam = new URLSearchParams();
    searchParam.set(this.primary_key, 'eq.'.concat(id));
    this.authHttp.delete(`${this.writeUrl}`, searchParam)
      .map((response: Response) => <any>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  put(data:any, onNext: (data: any) => void) {
    if (!data.hasOwnProperty('id')) {
      return;
    }

    let searchParam = new URLSearchParams();
    searchParam.set(this.primary_key, 'eq.'.concat(data.id));
    this.options.search = searchParam;

    this.authHttp.patch(this.writeUrl, data, this.options)
      .map((response: Response) => <any>response.json())
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  pollByProjectId(project: number, onNext?: (data: any[]) => void) {
    this.stop();

    let searchParam = new URLSearchParams();
    searchParam.set('project_id', `eq.${project}`);
    this.options.search = searchParam;

    let timer = Observable
      .timer(0, this.interval)
      .flatMap(() => this.authHttp.get(this.viewUrl, this.options))
      .map(response => <any[]>response.json())
      .retry(2);

    this.subscription = timer
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.view}`, error)
      );
  }

  stop() {
    console.log('stop observable');
    if (this.subscription) this.subscription.unsubscribe();
  }

}
