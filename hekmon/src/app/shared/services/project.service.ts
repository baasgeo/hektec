import {Injectable, Inject} from "@angular/core";
import {Response, Headers, RequestOptions, URLSearchParams} from "@angular/http";

import { AuthHttp } from 'angular2-jwt';
import {Project} from "../models/";

import {APP_CONFIG} from "../../app.config";
import {Observable} from "rxjs";

@Injectable()
export class ProjectService {
  viewUrl: string;
  writeUrl: string;
  view: string = 'webapp_project';
  table: string = 'project';
  interval: number = 5*60*1000;

  private subscription;

  constructor(@Inject(APP_CONFIG) private config, private authHttp: AuthHttp) {
    this.viewUrl = `${config.apiEndpoint}${this.view}`;
    this.writeUrl = `${config.apiEndpoint}${this.table}`;
  }

  // get(identifier: number) {
  //   return this.authHttp
  //     .get(`${this.url}/${identifier}`)
  //     .map((response: Response) => {
  //       console.log(response.json());
  //       return response.json()[0] as Project
  //     })
  //     .catch(this.handleError);
  // }

  // getAll() {
  //   console.log('getAll');
  //   // Instead of this.http, we'll use the this.authHttp method. Everything else remains the same.
  //   return this.authHttp
  //     .get(this.url)
  //     .map((response: Response) => {
  //       console.log(response.json());
  //       return response.json() as Project[]
  //     })
  //     .catch(this.handleError);
  // }

  get(identifier: number, onNext: (data: Project) => void) {
    this.authHttp
      .get(`${this.viewUrl}/${identifier}`)
      .map(response => <Project>response.json()[0])
      .retry(2)
      .subscribe(onNext,
        error =>
          error => console.log(`An error occurred when requesting ${this.view}`, error)
      );
  }

  getAll(onNext: (data: Project[]) => void, identifiers?: number[]) {
    let request = `${this.viewUrl}`;
    if (identifiers) {
      request = `${this.viewUrl}?id=in.${identifiers}`;
    }
    this.authHttp
      .get(request)
      .map(response => <Project[]>response.json())
      .retry(2)
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.view}`, error)
      );
  }

  getCSV(project: number, onNext: (data: any) => void) {
    let headers = new Headers({ 'Accept': 'text/csv' });
    let options = new RequestOptions({ headers: headers });

    this.authHttp
      .get(`${this.viewUrl}?project_id=eq.${project}`, options)
      .map(response => <any>response.text())
      .retry(2)
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  add(data:any, onNext: (data: any) => void) {
    Object.keys(data).forEach((key) => (data[key] == '') && delete data[key]);
    return this.authHttp.post(`${this.writeUrl}`, data)
      .map((response: Response) => <any>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  remove(primary_key:string, id:string, onNext: (data: any) => void) {
    let searchParam = new URLSearchParams();
    searchParam.set(primary_key, 'eq.'.concat(id));
    return this.authHttp.delete(`${this.writeUrl}`, searchParam)
      .map((response: Response) => <any>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  put(primary_key:string, data:any, onNext: (data: any) => void) {
    let searchParam = new URLSearchParams();
    searchParam.set(primary_key, 'eq.'.concat(data.message_uuid));
    return this.authHttp.patch(`${this.writeUrl}`, data, searchParam)
      .map((response: Response) => <any>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  poll(identifier: number, onNext: (data: Project) => void) {
    let timer = Observable
      .timer(0, this.interval)
      .flatMap(() => this.authHttp.get(`${this.viewUrl}/${identifier}`))
      .map(response => <Project>response.json()[0])
      .retry(2);

    this.subscription = timer
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.view}`, error)
      );
  }

  stop() {
    console.log('stop observable');
    if (this.subscription) this.subscription.unsubscribe();
  }

}
