import {Inject, Injectable} from "@angular/core";
import {Headers, RequestOptions, URLSearchParams} from "@angular/http";
import {AuthHttp} from "angular2-jwt";
import {APP_CONFIG} from "../../app.config";
import {Observable} from "rxjs/Rx";

@Injectable()
export class DeformationService {
  // As simple as possible service with time and callback
  //
  // http://davidpine.net/blog/angular-2-http/
  // https://blog.thoughtram.io/angular/2016/06/16/cold-vs-hot-observables.html
  // http://stackoverflow.com/questions/36600291/angular2-stop-cancel-clear-observable-in-routerondeactivate
  viewUrl: string;
  viewTable: string = 'webapp_deformation';
  adminTable: string = 'webapp_deformation_all';
  //period: string = '2017-02-08 00:00:00';//'today';
  interval: number = 5 * 60 * 1000;

  private subscription;

  constructor(@Inject(APP_CONFIG) private config,
              private authHttp: AuthHttp) {
    this.viewUrl = `${config.apiEndpoint}${this.viewTable}`;
  }

  getBySensorId(sensor: number, run: string, onNext: (data: any[]) => void) {
    this.authHttp
      .get(`${this.viewUrl}?sensor_id=eq.${sensor}&run=gte.${run}`)
      .map(response => <any[]>response.json())
      .retry(2)
      .subscribe(onNext,
        error =>
          error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  getCsvByProjectId(project: number, options: any, onNext: (data: any[]) => void) {
    let headers = new Headers({'Accept': 'text/csv'});
    let requestOptions = new RequestOptions({headers: headers});
    let request = `${this.viewUrl}?project_id=eq.${project}`;

    if (options && options.runBegin && options.runEnd) {
      request = `${request}&run=gte.${options.runBegin}&run=lte.${options.runEnd}`;
    }
    if (options && options.applianceId) {
      request = `${request}&appliance_id=eq.${options.applianceId}`;
    }

    this.authHttp
      .get(request, requestOptions)
      .map(response => <any>response.text())
      .retry(2)
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  getCsvBySensorId(sensor: number, runBegin: string, runEnd: string, onNext: (data: any[]) => void) {
    let headers = new Headers({'Accept': 'text/csv'});
    let options = new RequestOptions({headers: headers});

    this.authHttp
      .get(`${this.viewUrl}?sensor_id=eq.${sensor}&run=gte.${runBegin}&run=lte.${runEnd}`, options)
      .map(response => <any>response.text())
      .retry(2)
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  pollBySensorId(sensor: number, options: any, onNext: (data: any[]) => void) {
    this.stop();

    let params: URLSearchParams = new URLSearchParams();
    params.set('sensor_id', `eq.${sensor}`);

    if (options && options.hasOwnProperty('run')) {
      params.set('run', options['run']);
    }

    if (options && options.hasOwnProperty('showOmit')) {
      this.viewUrl = `${this.config.apiEndpoint}${this.adminTable}`;
    } else {
      this.viewUrl = `${this.config.apiEndpoint}${this.viewTable}`;
    }

    let timer = Observable
      .timer(0, this.interval)
      .flatMap(() => this.authHttp.get(this.viewUrl, {search: params}))
      .map(response => <any[]>response.json())
      .retry(2);

    this.subscription = timer
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  stop() {
    console.log('stop observable');
    if (this.subscription) this.subscription.unsubscribe();
  }

}
