import {Inject, Injectable} from "@angular/core";
import {Headers, RequestOptions, URLSearchParams} from "@angular/http";

import {AuthHttp} from "angular2-jwt";
import {Sensor} from "../models/";

import {APP_CONFIG} from "../../app.config";
import {Observable} from "rxjs";

@Injectable()
export class SensorService {
  viewUrl: string;
  viewTable: string = 'webapp_sensor';
  adminTable: string = 'webapp_sensor_all';
  interval: number = 5 * 60 * 1000;

  private subscription;

  constructor(@Inject(APP_CONFIG) private config,
              private authHttp: AuthHttp) {
    this.viewUrl = `${config.apiEndpoint}${this.viewTable}`;
  }

  get(identifier: number, onNext: (data: Sensor) => void) {
    this.authHttp
      .get(`${this.viewUrl}/${identifier}`)
      .map(response => <Sensor>response.json()[0])
      .retry(2)
      .subscribe(onNext,
        error =>
          error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  getByProjectId(project: number, options: any, onNext: (data: any[]) => void) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('project_id', `eq.${project}`);

    if (options && options.hasOwnProperty('getOmit')) {
      this.viewUrl = `${this.config.apiEndpoint}${this.adminTable}`;
    }

    this.authHttp
      .get(this.viewUrl, {search: params})
      .map(response => <Sensor[]>response.json())
      .retry(2)
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  getCSV(project: number, options: any, onNext: (data: any[]) => void) {
    let headers = new Headers({'Accept': 'text/csv'});
    let requestOptions = new RequestOptions({headers: headers});
    let request = `${this.viewUrl}?project_id=eq.${project}`;

    if (options && options.applianceId) {
      request = `${request}&appliance_id=eq.${options.applianceId}`;
    }

    this.authHttp
      .get(request, requestOptions)
      .map(response => <any>response.text())
      .retry(2)
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  pollByProjectId(project: number, options: any, onNext?: (data: any[]) => void) {
    this.stop();

    let params: URLSearchParams = new URLSearchParams();
    params.set('project_id', `eq.${project}`);

    if (options && options.hasOwnProperty('showOmit')) {
      this.viewUrl = `${this.config.apiEndpoint}${this.adminTable}`;
    } else {
      this.viewUrl = `${this.config.apiEndpoint}${this.viewTable}`;
    }

    if (options && options.hasOwnProperty('applianceId')) {
      params.set('appliance_id', `eq.${options.applianceId}`);
    }
    if (options && options.hasOwnProperty('applianceActive')) {
      params.set('appliance_active', `is.${options.applianceActive}`);
    }

    let timer = Observable
      .timer(0, this.interval)
      .flatMap(() => this.authHttp.get(this.viewUrl, {search: params}))
      .map(response => <any[]>response.json())
      .retry(2);

    this.subscription = timer
      .subscribe(onNext,
        error => console.log(`An error occurred when requesting ${this.viewTable}`, error)
      );
  }

  stop() {
    console.log('stop observable');
    if (this.subscription) this.subscription.unsubscribe();
  }

}
