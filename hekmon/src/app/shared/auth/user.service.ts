import {Injectable, Inject} from '@angular/core';
import { Response } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {APP_CONFIG} from "../../app.config";

// @flow
export class User {
  id: number;
  name: string;
  email: string;
  role: string;
  token: string;
  projects: number[];
}

@Injectable()
export class UserService {

  constructor(@Inject(APP_CONFIG) private config,
              private authHttp: AuthHttp) {
    this.url = `${config.apiEndpoint}${this.table}`;
  }

  url: string;
  table: string = 'users';

  handleError(err) {
    Observable.throw(err.json().error || 'Server error');
  }

  query(onNext: (data: User[]) => void) {
    this.authHttp
      .get(`${this.url}`)
      .map(response => <User[]>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  get(userId: number, onNext: (data: User) => void) {
    this.authHttp
      .get(`${this.url}/${userId}`)
      .map(response => <User>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  create(user: User, onNext: (data: User) => void) {
    return this.authHttp.post(this.url, user)
      .map(response => <User>response.json()[0])
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  changePassword(user, oldPassword, newPassword, onNext: (data: any) => void) {
    return this.authHttp.put(`${this.url}${user.id || user._id}/password`, {oldPassword, newPassword})
      .map((res:Response) => res.json())
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }

  remove(user, onNext: (data: any) => void) {
    return this.authHttp.delete(`${this.url}${user.id}`)
      .map(() => user)
      .subscribe(onNext,
        error =>
          console.log("An error occurred when requesting server: ", error));
  }
}
