import {NgModule} from '@angular/core';
import {AuthService} from './auth.service';
import {UserService} from './user.service';
import {AuthGuard} from "./guard.service";

@NgModule({
  providers: [
    AuthGuard,
    AuthService,
    UserService
  ]
})
export class AuthModule {
}
