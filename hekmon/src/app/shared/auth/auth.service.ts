import {Injectable, EventEmitter, Output, Inject} from "@angular/core";
import {Headers, RequestOptions, Http} from "@angular/http";
import {JwtHelper, tokenNotExpired} from "angular2-jwt";
import {User, UserService} from "./user.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/toPromise";
import {extractData} from "../util";
import {APP_CONFIG} from "../../app.config";

@Injectable()
export class AuthService {

  @Output() currentUserChanged = new EventEmitter(true);
  userRoles = [];

  private _currentUser: User;
  private jwtHelper: JwtHelper = new JwtHelper();
  private loginUrl: string;

  constructor(@Inject(APP_CONFIG) private config,
              private http: Http,
              private userService: UserService) {
    this.loginUrl = config.apiEndpoint + 'rpc/login';
    this.userRoles = config.userRoles;

    if (localStorage.getItem('token')) {
      let userSettings = this.jwtHelper.decodeToken(localStorage.getItem('token'));
      console.log(userSettings);
      this._currentUser = {
        email: userSettings.email,
        projects: userSettings.projects,
        role: userSettings.role
      } as User;
    }
  }

  /**
   * Check if userRole is >= role
   * @param {String} userRole - role of current user
   * @param {String} role - role to check against
   */
  hasRole(userRole, role) {
    return this.userRoles.indexOf(userRole) >= this.userRoles.indexOf(role);
  }

  get currentUser() {
    console.log(this._currentUser);
    return this._currentUser;
  }

  set currentUser(user: User) {
    this._currentUser = user;
    this.currentUserChanged.emit(user);
  }

  /**
   * Authenticate user and save token
   *
   * @param  {string}   username     - login info
   * @param  {string}   password     - login info
   * @return {Promise}
   */
  login(username: string, password: string): Observable<User> {
    let body = JSON.stringify({email: username, pass: password});
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});

    return this.http.post(this.loginUrl, body, options)
      .map(extractData)
      .map(res => {
        if (res && res[0].token) {
          console.log(res[0].token);
          let userSettings = this.jwtHelper.decodeToken(res[0].token);
          console.log(userSettings);
          localStorage.setItem('token', res[0].token);
          this.currentUser = {
            email: username,
            projects: userSettings.projects,
            role: userSettings.role
          } as User;
          return this.currentUser;
        }

      })
      .catch(err => {
        this.logout();
        return Observable.throw(err);
      });
  }

  /**
   * Delete access token and user info
   * @return {Promise}
   */
  logout() {
    localStorage.removeItem('token');
    this.currentUser = {} as User;
    return Observable.of();
  }

  /**
   * Create a new user
   *
   * @param  {Object}   user     - user info
   * @param  {Function} callback - optional, function(error, user)
   * @return {Promise}
   */
  createUser(user, callback) {
    return this.userService.create(user, user => {
      this.currentUser = user;
      localStorage.setItem('token', user.token);
    });
  }

  /**
   * Change password
   *
   * @param  {String}   oldPassword
   * @param  {String}   newPassword
   * @param  {Function} [callback] - function(error, user)
   * @return {Promise}
   */
  changePassword(oldPassword, newPassword, callback) {
    return this.userService.changePassword({id: this.currentUser.id}, oldPassword, newPassword, callback);
  }

  /**
   * Gets all available info on a user
   *
   * @param  {Function} [callback] - function(user)
   * @return {Promise}
   */
  getCurrentUser(callback) {
    //safeCb(callback)(this.currentUser);
    return Observable.of(this.currentUser);
  }

  /**
   * Gets all available info on a user
   *
   * @return {Object}
   */
  getCurrentUserSync() {
    return this.currentUser;
  }

  /**
   * Checks if user is logged in
   * @returns {Promise}
   */
  isLoggedIn(callback) {
    let is = this.currentUser.hasOwnProperty('role');
    //safeCb(callback)(is);
    return Observable.of(is);
  }

  /**
   * Checks if user is logged in
   * @returns {Boolean}
   */
  isLoggedInSync() {
    return tokenNotExpired('token');
  }

  /**
   * Check if a user is an admin
   *
   * @param  {Function|*} callback - optional, function(is)
   * @return {Promise}
   */
  isAdmin(callback) {
    return this.getCurrentUser(null)
      .map(user => {
        var is = user.role === 'pgr_authenticator';
        //safeCb(callback)(is);
        return is;
      });
  }

  isAdminSync() {
    return this.currentUser && this.currentUser.role === 'pgr_authenticator';
  }

  /**
   * Get auth token
   *
   * @return {String} - a token string used for authenticating
   */
  getToken() {
    return localStorage.getItem('token');
  }
}
