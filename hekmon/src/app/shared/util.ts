/**
 * The Util service is for thin, globally reusable, utility functions
 */

import { Response } from '@angular/http';
import * as fileSaver from "file-saver";

/**
 * Parse a given url with the use of an anchor element
 *
 * @param  {String} url - the url to parse
 * @return {Object}     - the parsed url, anchor element
 */
export function urlParse(url) {
  var a = document.createElement('a');
  a.href = url;

  // Special treatment for IE, see http://stackoverflow.com/a/13405933 for details
  if (a.host === '') {
    a.href = a.href;
  }

  return a;
}

export function extractData(res: Response) {
  if(!res.text()) return {};
  return res.json() || { };
}

export function toFileSaver(data: any, filename: string) {
  let blob = new Blob(['sep=,', '\r\n', data], {
    type: 'text/csv;charset=utf-8'
  });
  fileSaver.saveAs(blob, filename);
}
