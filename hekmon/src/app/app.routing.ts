import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import {AuthGuard} from "./shared/auth/guard.service";

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/pages/login',
    pathMatch: 'full',
  },
  {
    path: 'admin',
    component: FullLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'monitoring',
    component: FullLayoutComponent,
    data: {
      title: 'Monitoring'
    },
    children: [
      {
        path: '',
        loadChildren: './monitoring/monitoring.module#MonitoringModule',
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
