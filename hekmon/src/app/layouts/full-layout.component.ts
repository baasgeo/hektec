import { Component, OnInit } from '@angular/core';

import {AppService} from "../app.service";
import {AuthService} from "../shared/auth/auth.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  constructor(public appService: AppService, private auth: AuthService) { }

  disabled:boolean = false;

  isAdmin: boolean= this.auth.isAdminSync();
  userName: string = this.auth.currentUser.email;
  multipleProjects: boolean = this.auth.currentUser.projects.length > 1;


  logout() {
    this.auth.logout();
  }

  ngOnInit(): void {}
}
