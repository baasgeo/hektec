import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  imports: [
    NgbModule,
    FormsModule,
    UsersRoutingModule
  ],
  declarations: [ UsersComponent ]
})
export class UsersModule { }
