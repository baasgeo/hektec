import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataFilter'
})
export class DataFilterPipe implements PipeTransform {

  transform(array: any[], query: string): any {
    if (query) {
      return array.filter(row=>row.identity.indexOf(query) > -1);
      // query = query ? query.toLowerCase() : '';
      // return query && array ?
      //   array.filter(item =>
      //     (item.identity.toLowerCase().indexOf(query) !== -1) ||
      //     (item.title.toLowerCase().indexOf(query) !== -1)
      //   ) : array;
    }
    return array;
  }
}
