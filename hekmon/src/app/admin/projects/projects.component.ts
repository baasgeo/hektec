import { Component } from '@angular/core';
import {Project} from "../../shared/models/project";
import {ProjectService} from "../../shared/services/project.service";
import {AppService} from "../../app.service";

@Component({
  templateUrl: './projects.component.html'
})
export class ProjectsComponent {
  constructor(private projectService: ProjectService,
              private appService: AppService) { }

  projects: Project[];
  filterQuery = '';

  ngOnInit(): void {
    this.projectService.getAll(projects => this.projects = projects);
  }

}
