import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";


import { ProjectsComponent } from './projects.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import {DataFilterPipe} from "./datafilterpipe";
import {DataTableModule} from "angular2-datatable";

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    DataTableModule,
    ProjectsRoutingModule
  ],
  declarations: [
    DataFilterPipe,
    ProjectsComponent
  ]
})
export class ProjectsModule { }
