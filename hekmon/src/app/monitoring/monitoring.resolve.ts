import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {AppService} from "../app.service";

@Injectable()
export class MonitoringResolve implements Resolve<any> {

  constructor(private appService: AppService) {}

  resolve(route: ActivatedRouteSnapshot) {
    console.log('Monitoring resolve...', route.params['id']);
    this.appService.projectId = +route.params['id'];
    return +route.params['id'];
  }
}
