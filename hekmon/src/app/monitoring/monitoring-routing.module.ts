import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MonitoringResolve} from "./monitoring.resolve";

const routes: Routes = [
  {
    path: ':id',
    resolve: {
      id: MonitoringResolve
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'report',
        loadChildren: './report/report.module#ReportModule'
      },
      {
        path: 'appliance',
        loadChildren: './appliance/appliance.module#ApplianceModule'
      },
      {
        path: 'sensors',
        loadChildren: './sensors/sensors.module#SensorsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringRoutingModule {}
