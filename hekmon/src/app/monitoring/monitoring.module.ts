import { NgModule } from '@angular/core';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import {MonitoringResolve} from "./monitoring.resolve";

@NgModule({
  imports: [
    MonitoringRoutingModule ],
  providers: [MonitoringResolve]
})
export class MonitoringModule { }
