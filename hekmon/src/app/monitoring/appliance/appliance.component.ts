import {Component} from "@angular/core";
import {AppService} from "../../app.service";
import {ApplianceService} from "../../shared/services/appliance.service";
import {Router} from "@angular/router";
import {AuthService} from "../../shared/auth/auth.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  templateUrl: './appliance.component.html'
})
export class ApplianceComponent {

  constructor(private appService: AppService,
              private router: Router,
              private auth: AuthService,
              private applianceService: ApplianceService,
              private modalService: NgbModal) {
  }

  currentAppliance: any;
  appliances: any[];
  isAdmin: boolean = this.auth.isAdminSync();

  private projectId: number;

  ngOnInit(): void {
    this.projectId = this.appService.projectId;

    this.poll();
  }

  edit(appliance: any, content) {
    this.currentAppliance = appliance;
    this.currentAppliance.maintenance = !appliance.active;

    this.modalService.open(content).result.then((result) => {
      console.log(`Closed with: ${result}`);

      const newAppliance = {
        id: appliance.id,
        title: appliance.title,
        description: appliance.description,
        notes: appliance.notes,
        active: !appliance.maintenance
      };

      this.applianceService.put(newAppliance, response => {
        console.log(response);
        this.poll();
      });
    }, (reason) => {
      console.log(`Dismissed ${reason}`);
    });

  }

  navigateTo(value: number) {
    this.appService.applianceId = value;
    this.router.navigate(['monitoring', this.projectId, 'sensors']);
  }

  ngOnDestroy() {
    this.applianceService.stop();
  }

  private poll() {
    this.applianceService.pollByProjectId(this.projectId, appliances => {
      this.appliances = appliances;
    });
  }

}
