import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { ApplianceComponent } from './appliance.component';
import { ApplianceRoutingModule } from './appliance-routing.module';
import {DataTableModule} from "angular2-datatable";

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    DataTableModule,
    ApplianceRoutingModule
  ],
  declarations: [
    ApplianceComponent
  ]
})
export class ApplianceModule { }
