import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplianceComponent } from './appliance.component';

const routes: Routes = [
  {
    path: '',
    component: ApplianceComponent,
    data: {
      title: 'Instrument'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplianceRoutingModule {}
