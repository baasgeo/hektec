import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import {ProjectService} from "../../shared/services/project.service";
import {AppService} from "../../app.service";
import {Project} from "../../shared/models/";

@Component({
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  constructor(private projectService: ProjectService,
              private appService: AppService,
              private router: Router) { }

  project: Project;
  projectId: number;

  ngOnInit(): void {
    this.projectId = this.appService.projectId;
    this.projectService.poll(this.projectId, project => this.project = project);
  }

  navigateTo(name: string): void {
    this.router.navigate(['/monitoring', this.projectId, name]);
  }

  ngOnDestroy() {
    this.projectService.stop();
  }
}
