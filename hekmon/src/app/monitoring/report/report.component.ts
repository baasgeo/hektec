import {Component} from "@angular/core";

import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {DeformationService} from "../../shared/services/deformation.service";
import {AppService} from "../../app.service";
import {SensorService} from "../../shared/services/sensor.service";

import * as moment from "moment";
import {ApplianceService} from "../../shared/services/appliance.service";
import * as util from "../../shared/util"
const now = new Date();

@Component({
  templateUrl: './report.component.html'
})
export class ReportComponent {

  constructor(private appService: AppService,
              private applianceService: ApplianceService,
              private sensorService: SensorService,
              private deformationService: DeformationService) {
  }

  startDate: NgbDateStruct;
  endDate: NgbDateStruct;
  downloadType: string = 'detail';
  downloadAppliance: string = '';
  downloading = false;
  appliances: any[];
  private projectId: number;

  ngOnInit(): void {
    this.projectId = this.appService.projectId;
    this.endDate = this.getToday();

    this.applianceService.getByProjectId(this.projectId, appliances => {
      this.appliances = appliances;
    });
  }

  getToday(): NgbDateStruct {
    return {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }

  getYesterday(): NgbDateStruct {
    return {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1};
  }

  getCSV() {
    this.downloading = true;
    if (this.downloadType == 'detail') {
      const begin = moment(new Date(this.startDate.year, this.startDate.month - 1, this.startDate.day)).toDate().toDateString();
      const end = moment(new Date(this.endDate.year, this.endDate.month - 1, this.endDate.day + 1)).toDate().toDateString();

      console.log(begin, end);
      this.deformationService.getCsvByProjectId(this.projectId, {
        runBegin: begin,
        runEnd: end,
        applianceId: this.downloadAppliance
      }, data => {
        this.toFileSaver(data, `deformations_${this.projectId}.csv`);
      });
    }
    else if (this.downloadType == 'detailAll') {
      this.deformationService.getCsvByProjectId(this.projectId, {applianceId: this.downloadAppliance}, data => {
        this.toFileSaver(data, `deformations_${this.projectId}.csv`);
      });
    }
    else if (this.downloadType == 'overview') {
      this.sensorService.getCSV(this.projectId, {applianceId: this.downloadAppliance}, data => {
        this.toFileSaver(data, `sensors_${this.projectId}.csv`);
      });
    }

  }

  private toFileSaver(data: any, filename: string) {
    util.toFileSaver(data, filename);
    this.downloading = false;
  }

}
