import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Sensors'
    },
    children: [
      {
        path: '',
        loadChildren: './overview/overview.module#SensorsModule'
      },
      {
        path: ':id',
        loadChildren: './detail/sensor_detail.module#SensorDetailModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SensorsRoutingModule {}
