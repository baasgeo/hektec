import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Sensor} from '../../../shared/models/sensor';
import {SensorService} from '../../../shared/services/sensor.service';
import {DeformationService} from '../../../shared/services/deformation.service';
import {AppService} from '../../../app.service';
import * as moment from 'moment';
import {AuthService} from '../../../shared/auth/auth.service';

declare const Chart: any;

@Component({
  templateUrl: './sensor_detail.component.html'
})
export class SensorDetailComponent implements OnInit {

  @ViewChild('dateTmpl') dateTemplateRef: TemplateRef<any>;
  sensor: Sensor;
  sensorCount: number;
  sensorData: any[];
  columns = [];
  isAdmin: boolean = this.auth.isAdminSync();
  hideTable: boolean = true;
  hideRef: boolean = false;
  hidePref: boolean = false;
  private projectId: number;
  private sensorId: number;
  private chartjs1;
  private chartjs2;
  private replaceChart = true;
  private options: any = {};
  // Labels
  private xLabel = ['∆ oost'];
  private yLabel = ['∆ noord'];
  private zlabel = ['∆ hoogte'];

  constructor(private appService: AppService,
              private route: ActivatedRoute,
              private auth: AuthService,
              private sensorService: SensorService,
              private deformationService: DeformationService) {
  }

  get run(): string {
    return this.options.run;
  }

  set run(value: string) {
    this.options.run = value;
    if (value == 'week') {
      value = moment().startOf('day').subtract(1, 'week').toDate().toDateString();
    }
    if (value == 'month') {
      value = moment().startOf('day').subtract(1, 'month').toDate().toDateString();
    }
    this.replaceChart = true;
    this.startPoll(value, this.showOmit);
  }

  get showOmit(): boolean {
    return this.options.showOmit;
  }

  set showOmit(value: boolean) {
    this.options.showOmit = value;
    this.startPoll(this.run, value);
  }

  // call the getnames function to fetch data from json
  ngOnInit() {
    this.projectId = this.appService.projectId;
    this.sensorId = +this.route.snapshot.params['id'];

    // defaults
    this.options.run = 'yesterday'; //"2017-02-07 00:00:00"
    this.options.showOmit = false;

    this.sensorService.get(this.sensorId, sensor => {
      this.sensor = sensor;
    });

    this.startPoll(this.run);

    this.columns = [
      {prop: 'run', name: 'Datum', cellTemplate: this.dateTemplateRef},
      {prop: 'delta_ref_east', name: '∆oost(R)', width: 50},
      {prop: 'delta_ref_north', name: '∆noord(R)', width: 50},
      {prop: 'delta_ref_elevation', name: '∆hoogte(R)', width: 50}
    ];
  }

  startPoll(run, showOmit?) {
    const options = {};
    if (run != '∞') {
      options['run'] = `gte.${run}`;
    }
    if (showOmit) options['showOmit'] = showOmit;

    this.deformationService.pollBySensorId(this.sensorId, options, data => {
      this.sensorCount = data.length;
      this.sensorData = data;
      this.renderCharts(data);
      this.replaceChart = false;
    });
  }

  ngOnDestroy() {
    this.deformationService.stop();
  }

  private renderCharts(sensorData: any[]) {
    console.log('got sensor data, count: ', sensorData.length);

    if (this.replaceChart) {
      this.prepareChartData(sensorData);
    } else {
      this.updateChartjs(sensorData);
    }
  }

  private prepareChartData(sensorData: any[]) {
    let deltaRefX = [], deltaRefY = [], deltaRefZ = [];
    let deltaPrevX = [], deltaPrevY = [], deltaPrevZ = [];
    let deltaABS = [];

    sensorData.forEach(function (d, i) {

      deltaRefX.push({
        x: moment(d.run).toDate(),
        y: d.delta_ref_east
      });
      deltaRefY.push({
        x: moment(d.run).toDate(),
        y: d.delta_ref_north
      });
      deltaRefZ.push({
        x: moment(d.run).toDate(),
        y: d.delta_ref_elevation
      });

      // deltaPrevX.push({
      //   x: moment(d.run).toDate(),
      //   y: d.delta_prev_east
      // });
      // deltaPrevY.push({
      //   x: moment(d.run).toDate(),
      //   y: d.delta_prev_north
      // });
      // deltaPrevZ.push({
      //   x: moment(d.run).toDate(),
      //   y: d.delta_prev_elevation
      // });
      //
      // deltaABS.push({
      //   x: moment(d.run).toDate(),
      //   y: Math.abs(d.delta_ref_east) + Math.abs(d.delta_ref_north) + Math.abs(d.delta_ref_elevation)
      // });
    });

    if (this.chartjs1) {
      this.chartjs1.destroy();
    }
    if (this.chartjs2) {
      this.chartjs2.destroy();
    }

    this.chartjs1 = this.createChartjs('myChart_delta_rev', deltaRefX , deltaRefY , deltaRefZ);
    //this.chartjs2 = this.createChartjs('myChart_delta_prev', deltaPrevX , deltaPrevY , deltaPrevZ);
    //this.chartjs2 = this.createChartjs('myChart_delta_prev', deltaABS);
  }

  private createChartjs(id, xData, yData?, zData?) {
    // http://www.chartjs.org/docs/latest/configuration/elements.html
    Chart.defaults.global.elements.line.fill = false;

    Chart.defaults.global.elements.point.radius = 0;
    Chart.defaults.global.elements.line.borderWidth = 1;
    Chart.defaults.global.elements.line.tension = 0;

    let unit = 'hour';

    if (this.run == 'today'){
      Chart.defaults.global.elements.point.radius = 3;
      Chart.defaults.global.elements.line.borderWidth = 2;
      Chart.defaults.global.elements.line.tension = 0.3;
    } else if (this.run == 'yesterday'){
      Chart.defaults.global.elements.point.radius = 2;
      Chart.defaults.global.elements.line.borderWidth = 2;
      Chart.defaults.global.elements.line.tension = 0.2;
    } else if (this.run == 'week'){
      unit = 'day';
      Chart.defaults.global.elements.point.radius = 1;
      Chart.defaults.global.elements.line.borderWidth = 1;
      Chart.defaults.global.elements.line.tension = 0.1;
    } else if (this.run == 'month'){
      unit = 'week';
    } else {
      unit = '';
    }

    let xAxes = [{
      type: 'time',
      time: {
        bounds: 'data',
        source: 'data',
        round: true,
        distribution: 'series',
        isoWeekday: true,
        unit: unit,
        displayFormats: {
          minute: 'H:mm',
          hour: 'H:mm',
          day: 'MMM D'
        }
      },
      scaleLabel: {
        display: true,
        labelString: 'Moment'
      }
    }];

    let ctx = document.getElementById(id);
    return new Chart(ctx, {
      type: 'line',
      data: {
        datasets: [
          {
            data: xData,
            label: this.xLabel,
            borderColor: 'rgb(6, 197, 172)',
            pointBackgroundColor: 'rgb(6, 197, 172)'
          },
          {
            data: yData,
            label: this.yLabel,
            borderColor: 'rgb(63, 170, 227)',
            pointBackgroundColor: 'rgb(63, 170, 227)'
          },
          {
            data: zData,
            label: this.zlabel,
            borderColor: 'rgb(238, 99, 76)',
            pointBackgroundColor: 'rgb(238, 99, 76)'
          }
        ]
      },
      options: {
        //responsive: true,
        //responsiveAnimationDuration: 0,
        scales: {
          xAxes: xAxes,
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Millimeters'
            }
          }]
        },
        legend: {
          display: true,
          position: 'bottom'
        }
      }
    });
  }

  private updateChartjs(sensorData: any[]) {
    let deltaRefX = [];
    let deltaRefY = [];
    let deltaRefZ = [];
    sensorData.forEach(function (d, i) {

      deltaRefX.push({
        x: moment(d.run).toDate(),
        y: d.delta_ref_east
      });
      deltaRefY.push({
        x: moment(d.run).toDate(),
        y: d.delta_ref_north
      });
      deltaRefZ.push({
        x: moment(d.run).toDate(),
        y: d.delta_ref_elevation
      });
    });

    this.chartjs1.data.datasets[0].data = deltaRefX;
    this.chartjs1.data.datasets[1].data = deltaRefY;
    this.chartjs1.data.datasets[2].data = deltaRefZ;

    this.chartjs1.update(0);
  }

}
