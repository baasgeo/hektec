import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SensorDetailComponent } from './sensor_detail.component';

const routes: Routes = [
  {
    path: '',
    component: SensorDetailComponent,
    data: {
      title: 'Detail'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SensorDetailRoutingModule {}
