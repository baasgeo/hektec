import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";

import { SensorDetailComponent } from './sensor_detail.component';
import { SensorDetailRoutingModule } from './sensor_detail-routing.module';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    SensorDetailRoutingModule
  ],
  declarations: [ SensorDetailComponent ]
})
export class SensorDetailModule { }
