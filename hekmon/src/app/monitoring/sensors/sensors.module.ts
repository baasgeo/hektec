import { NgModule } from '@angular/core';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { SensorsRoutingModule } from './sensors-routing.module';

@NgModule({
  imports: [
    NgbModule,
    SensorsRoutingModule
  ],
  declarations: [ ]
})
export class SensorsModule { }
