import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SensorsComponent } from './overview.component';

const routes: Routes = [
  {
    path: '',
    component: SensorsComponent,
    data: {
      title: 'Overview'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SensorsRoutingModule {}
