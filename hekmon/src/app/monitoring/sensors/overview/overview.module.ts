import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from '@angular/forms';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";

import { SensorsComponent } from './overview.component';
import { SensorsRoutingModule } from './overview-routing.module';


@NgModule({
  imports: [
    NgbModule,
    FormsModule,
    CommonModule,
    NgxDatatableModule,
    SensorsRoutingModule
  ],
  declarations: [ SensorsComponent ]
})
export class SensorsModule { }
