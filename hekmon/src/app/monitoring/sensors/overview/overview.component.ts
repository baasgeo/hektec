import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

import {SensorService} from "../../../shared/services/sensor.service";
import {Sensor} from "../../../shared/models/sensor";
import {AppService} from "../../../app.service";
import {ApplianceService} from "../../../shared/services/appliance.service";
import * as util from "../../../shared/util"
import {AuthService} from "../../../shared/auth/auth.service";

@Component({
  templateUrl: './overview.component.html'
})
export class SensorsComponent implements OnInit, OnDestroy {

  constructor(private appService: AppService,
              private router: Router,
              private route: ActivatedRoute,
              private auth: AuthService,
              private applianceService: ApplianceService,
              private sensorService: SensorService) {
  }

  @ViewChild('linkTmpl') linkTmpl: TemplateRef<any>;
  @ViewChild('deltaTmpl') deltaTmpl: TemplateRef<any>;

  sensor: Sensor;
  sensors: Sensor[];
  appliances: any[];
  columns = [];
  isAdmin: boolean= this.auth.isAdminSync();

  private projectId: number;
  private options: any = {};


  ngOnInit(): void {
    this.projectId = this.appService.projectId;

    // defaults
    this.options.applianceId = this.appService.applianceId || '';
    this.options.showOmit;

    this.columns = [
      {prop: 'identity', name: 'Id'},
      // { prop: 'kind' },
      // { prop: 'title' },
      {prop: 'avg_delta_ref_east', name: '∆oost(R)', cellTemplate: this.deltaTmpl},
      {prop: 'avg_delta_ref_north', name: '∆noord(R)', cellTemplate: this.deltaTmpl},
      {prop: 'avg_delta_ref_elevation', name: '∆hoogte(R)', cellTemplate: this.deltaTmpl},
      {prop: 'avg_delta_prev_east', name: '∆oost(P)', cellTemplate: this.deltaTmpl},
      {prop: 'avg_delta_prev_north', name: '∆noord(P)', cellTemplate: this.deltaTmpl},
      {prop: 'avg_delta_prev_elevation', name: '∆hoogte(P)', cellTemplate: this.deltaTmpl},
      // { prop: 'notes' },
      {prop: 'id', name: ' ', cellTemplate: this.linkTmpl}
    ];

    this.pollSensors(this.options.applianceId);

    this.applianceService.getByProjectId(this.projectId, appliances => {
      if (this.isAdmin) {
        this.appliances = appliances;
      } else {
        this.appliances = appliances.filter(item => item.active);
      }

    });

  }

  get applianceId(): number {
    return this.options.applianceId;
  }

  set applianceId(value: number) {
    this.options.applianceId = value;
    this.appService.applianceId = value;

    this.pollSensors(value, this.showOmit);
  }

  get showOmit(): boolean {
    return this.options.showOmit;
  }

  set showOmit(value: boolean) {
    this.options.showOmit = value;
    this.pollSensors(this.applianceId, value);
  }

  getLimitColor(value: number, column, row): string {
    let fieldType = column.prop.split('_')[3];
    if (!row['boundary_id']) return '';
    else if (Math.abs(value) > row['danger_' + fieldType]) return 'red';
    else if (Math.abs(value) > row['alert_' + fieldType]) return 'orange';
    else return '';
  }

  isNumber(value) {
    return typeof value === 'number';
  }

  ngOnDestroy() {
    this.sensorService.stop();
  }

  // used to fetch all the data from json file
  setCurrentSensor(event) {
    this.sensor = event.data;
  }

  goToDetails(event) {
    this.router.navigate([event.data.id], {relativeTo: this.route});
  }

  private pollSensors(value?: number, showOmit?: boolean) {
    const options = {};
    if (value) options['applianceId'] = value;
    if (showOmit) options['showOmit'] = showOmit;
    if (!this.isAdmin) options['applianceActive'] = true;
    this.sensorService.pollByProjectId(this.projectId, options , sensors => {
      this.sensors = sensors;
    });
  }

}
