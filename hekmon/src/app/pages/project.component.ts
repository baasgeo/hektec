import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import {AppService} from "../app.service";
import {AuthService} from "../shared/auth/auth.service";
import {ProjectService} from "../shared/services/project.service";
import {Project} from "../shared/models/project";

@Component({
  templateUrl: './project.component.html'
})
export class ProjectComponent implements OnInit {

  constructor(public appService: AppService,
              private auth: AuthService,
              private projectService: ProjectService,
              private router: Router) { }

  projects: Project[];
  selectedProject: number;

  select(index) {
    this.selectedProject = index;
  }

  goToProject(project) {
    this.appService.projectId = project.id;
    this.router.navigate(['/monitoring', project.id, 'dashboard']);
  }

  ngOnInit() {
    let projects = this.auth.currentUser.projects;
    this.projectService.getAll(projects => {
      this.projects = projects;
    }, projects);
  }

}
