import {Component} from "@angular/core";
import {AppService} from "../app.service";
import {AuthService} from "../shared/auth/auth.service";
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {

  constructor(private appService: AppService,
              formBuilder: FormBuilder,
              private router: Router,
              private auth: AuthService) {

    this.loginForm = formBuilder.group({
      'email': [null, Validators.required],
      'password': [null, Validators.required],
    })

  }

  // We are going to declare our variables here. We’ll have a loginForm that will represent our reactive form, an authenticated boolean that will be true or false based on the users auth status and finally a profile object that will hold the user data.
  loginForm: FormGroup;
  authenticated: boolean;
  loading: boolean = false;
  errorMsg = undefined;
  profile: Object;

  submitForm(value: any){

    this.loading = true;
    this.auth.login(value.email,  value.password)
      .subscribe(user => {
        console.log(user);
        if (user) {
          console.log('credentials are correct, navigate...');
          if (user.projects.length > 1) {
            this.router.navigate(['/pages', 'project']);
          } else {
            this.appService.projectId = user.projects[0];
            this.router.navigate(['/monitoring', this.appService.projectId, 'dashboard']);
          }
        } else {
          this.errorMsg = 'Gebruiker onbekend of wachtwoord incorrect';
          this.loading = false;
        }
      });

  }

  logout() {
    this.auth.logout();
    this.authenticated = false;
  }

}
