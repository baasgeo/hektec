import { OpaqueToken } from "@angular/core";

export let APP_CONFIG = new OpaqueToken("app.config");

interface Config {
  apiEndpoint: string;
  userRoles: string[];
}

export const AppConfig: Config = {
  apiEndpoint: 'https://hektec.geoatlas.nl/rest/',
  userRoles: ['guest', 'user', 'admin']
};
