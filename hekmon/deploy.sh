ng build --target=production --environment=prod --aot
rm dist.zip
zip -r dist.zip dist
scp dist.zip sysop@hektec.geoatlas.nl:dist.zip
ssh sysop@hektec.geoatlas.nl rm -R dist
ssh sysop@hektec.geoatlas.nl unzip dist.zip
ssh sysop@hektec.geoatlas.nl rm -R /data/www/hekmon/*
ssh sysop@hektec.geoatlas.nl cp -R dist/* /data/www/hekmon
